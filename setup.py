from setuptools import setup, find_packages

setup(
    name='pytorch_wrapper',
    version='0.1',
    description='Pytorch wrapper for performing stuffs like learning rate finder, loss&metric callback, or parameters scheduling',
    author='Your friendly neighbourhood data science team',
    packages=find_packages(),  # install all packages
    install_requires=['torch==1.3.0'],
)